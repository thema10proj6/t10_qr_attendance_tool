## QR Attendance Tool
 By Kim Chau Duong & Rinze-Pieter Jonker 


## Purpose

A simple web application that takes student attendance using QR codes. 
Teachers can generate a QR code that will be displayed during class.
Students can scan it and be redirected to a page with a single web-form, 
where they can register attendance just by filling in their student number. 
Afterwards the teacher can acquire the attendance data by downloading the SQL database dump.

## Usage
* Run the `Attendance.sql` file in `src/main/resources/` with MySQL to create the 2 database tables (CheckIns and QRInstances).
* In the same directory sample data is provided in `TestData.sql`. Simply run the file  with MySQL to add these insertions.
* The `web.xml` file in `src/main/webapp/WEB-INF/` has configurable parameters in it. 
    * Change the `configured_password` parameter to change the password. Default is `testweb`.
    * Change the `sqlUser`, `sqlPassword`, `sqlHost`, `sqlDatabase` parameters with the right database credentials.
    * Change the `website_url` parameters with the website that's hosting this tool. Default is `http://localhost:8080`.
* Once the web application is deployed, the user can log in with the configured password.
* After logging in fill in the form that's displayed. It will ask for a teacher code, a course code and the number of students currently present.
* Pressing submit will redirect you to the page that displays the QR code. Students will have to scan this QR code.
* Scanning this code will redirect you to a page with a form asking for a student number. Entering your student number will register your attendance.
* On the QR code page a counter is shown underneath the QR code that shows the amount of student number submissions for that instance.
* Once the that counter has reached the limit the student number submissions will be disabled.
* The database dump can be downloaded by:
    * Heading over to the download page that's available in the navigation bar.
    * Heading over to the statistics page that's available on the QR code page.
* Either the entire attendance data dump can be downloaded or the attendance data of a specific lesson.
* Select the teacher code, course code and date of the lesson to download that specific data.
* The attendance data will be provided as a .csv file that's comma delimited.

## Library versions
```
| Library   | Version |
|-----------|---------|
| JDK       | 1.8_144 |
| JSTL      | 1.2     |
| Taglibs   | 1.1.2   |
| JDBC      | 5.1.6   |
| jQuery    | 3.3.1   |
| QRCode.js | 1.0     |
| Bootstrap | 3.3.7   |
| Popper.js | 1.14.6  |
```
