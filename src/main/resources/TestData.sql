/**
Run this file to add test data to your database.
 */
INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547407761, 1547686588, "TESTVAK", "TEST", 123456,  "6807c473-0a85-4b95-8dde-b3cb11c04655");
INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547407761, 1547407932, "TESTVAK", "TEST", 654321,  "6807c473-0a85-4b95-8dde-b3cb11c04655");
INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547407761, 1547407937, "TESTVAK", "TEST", 111111,  "6807c473-0a85-4b95-8dde-b3cb11c04655");

INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547410286, 1547410305, "TESTVAK", "TEST", 555,   "b0a507cc-5ce4-4a0a-8ce9-1dd2a8611f78");
INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547410286, 1547410310, "TESTVAK", "TEST", 123456, "b0a507cc-5ce4-4a0a-8ce9-1dd2a8611f78");

INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1548595089, 1548595567, "TESTVAK2", "TEST", 121212, "15d06201-d393-4415-9912-dba67d712601");

INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547410286, 1547410316, "COURSE1", "COUR", 986, "a792df6d-9b80-4517-ada6-ec0dd8ab8ea1");
INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547410286, 1547410471, "COURSE1", "COUR", 123456, "a792df6d-9b80-4517-ada6-ec0dd8ab8ea1");
INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547410286, 1547410420, "COURSE1", "COUR", 1000000,"a792df6d-9b80-4517-ada6-ec0dd8ab8ea1");
INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547410286, 1547410488, "COURSE1", "COUR", 99999,"a792df6d-9b80-4517-ada6-ec0dd8ab8ea1");

INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547432592, 1547432678, "COURSE2", "SNSD", 111, "21da3216-27c0-4ad1-bf24-ade7ca487173");
INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547432592, 1547432777, "COURSE2", "SNSD", 222, "21da3216-27c0-4ad1-bf24-ade7ca487173");
INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547432592, 1547432799, "COURSE2", "SNSD", 333, "21da3216-27c0-4ad1-bf24-ade7ca487173");
INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547432592, 1547432800, "COURSE2", "SNSD", 444, "21da3216-27c0-4ad1-bf24-ade7ca487173");
INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547432592, 1547432800, "COURSE2", "SNSD", 555, "21da3216-27c0-4ad1-bf24-ade7ca487173");

INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) VALUES
  (1547648313, 1547648389, "COURSE3", "SNSD", 666, "8ae93763-dcd6-41fe-a6cf-17bb9dabef23");


INSERT INTO QRInstances (qr_url, unix_one, teacher, course_code, max_count, hash_string) VALUES
  ("http://localhost:8080/attendance?&teachercode=test&coursecode=testvak&unixdate1=1547407761&hashstring=6807c473-0a85-4b95-8dde-b3cb11c04655",
   1547407761, "TEST", "TESTVAK", 3, "6807c473-0a85-4b95-8dde-b3cb11c04655");
INSERT INTO QRInstances (qr_url, unix_one, teacher, course_code, max_count, hash_string) VALUES
  ("http://localhost:8080/attendance?&teachercode=test&coursecode=testvak&unixdate1=1547410286&hashstring=b0a507cc-5ce4-4a0a-8ce9-1dd2a8611f78",
   1547410286, "TEST", "TESTVAK", 3, "b0a507cc-5ce4-4a0a-8ce9-1dd2a8611f78");
INSERT INTO QRInstances (qr_url, unix_one, teacher, course_code, max_count, hash_string) VALUES
  ("http://localhost:8080/attendance?&teachercode=test&coursecode=testvak2&unixdate1=1548595089&hashstring=15d06201-d393-4415-9912-dba67d712601",
   1548595089, "TEST", "TESTVAK2", 20, "15d06201-d393-4415-9912-dba67d712601");
INSERT INTO QRInstances (qr_url, unix_one, teacher, course_code, max_count, hash_string) VALUES
  ("http://localhost:8080/attendance?&teachercode=test&coursecode=testvak3&unixdate1=1548595171&hashstring=f4c49692-1fff-4f19-a740-85614aa6d105",
   1548595171, "TEST", "TESTVAK3", 20, "f4c49692-1fff-4f19-a740-85614aa6d105");
INSERT INTO QRInstances (qr_url, unix_one, teacher, course_code, max_count, hash_string) VALUES
  ("http://localhost:8080/attendance?&teachercode=cour&coursecode=course1&unixdate1=1547410286&hashstring=a792df6d-9b80-4517-ada6-ec0dd8ab8ea1",
   1547410286, "COUR", "COURSE1", 5, "a792df6d-9b80-4517-ada6-ec0dd8ab8ea1");
INSERT INTO QRInstances (qr_url, unix_one, teacher, course_code, max_count, hash_string) VALUES
  ("http://localhost:8080/attendance?&teachercode=snsd&coursecode=course2&unixdate1=1547432592&hashstring=21da3216-27c0-4ad1-bf24-ade7ca487173",
   1547432592, "SNSD", "COURSE2", 24, "21da3216-27c0-4ad1-bf24-ade7ca487173");
INSERT INTO QRInstances (qr_url, unix_one, teacher, course_code, max_count, hash_string) VALUES
  ("http://localhost:8080/attendance?&teachercode=snsd&coursecode=course3&unixdate1=1547648313&hashstring=8ae93763-dcd6-41fe-a6cf-17bb9dabef23",
   1547648313, "SNSD", "COURSE3", 1, "8ae93763-dcd6-41fe-a6cf-17bb9dabef23");

