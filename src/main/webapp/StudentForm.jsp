<%--
  Created by IntelliJ IDEA.
  User: kcduong
  Date: 12-12-18
  Time: 9:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Attendance</title>
    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css"/>

    <script type="text/javascript" src="js/lib/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="js/UnixGenerator.js" type="application/javascript"></script>
    <script src="js/HiddenValueAssigner.js" type="application/javascript"></script>
    <script src="js/SubmitDisabler.js" type="application/javascript"></script>
    <script src="js/ScrollFieldDisabler.js" type="application/javascript"></script>
</head>
<body>
    <jsp:include page="includes/EmptyHeader.jsp"></jsp:include>

    <div class="centered-fields">
        <div class="container" align="center">
            <h2>Please enter your student number to take attendance</h2>
            <form action="attendance_taken" method="get" onsubmit="unix()">
                <input type="hidden" value="" name="unixdate1" id="hidden_unixdate1" >
                <input type="hidden" name="unixdate2" id="hidden_unixdate" >
                <input type="hidden" value="" name="coursecode" id="hidden_course_code" >
                <input type="hidden" value="" name="teachercode" id="hidden_teacher_code" >
                <input type="hidden" value="" name="hashstring" id="hidden_hashstring">

                <div class="form-group">
                    <label for="studentno_field">Student number: </label> <br />
                    <input class="form-control resize-field" id="studentno_field" type="number" pattern="[0-9]*" min="0"
                           onkeydown="return event.keyCode !== 69" name="studentno" required title="Your Student number"
                           placeholder="Student number"/>
                </div>
                <div class="form-group">
                    <input class="btn btn-default form-control resize-field" type="submit" value="Submit" id="student_submit"/>
                </div>
            </form>
            <p id="full_attendance"> ${requestScope.full}</p>
            <p id="error">${requestScope.error}</p>
        </div>
    </div>
</body>
</html>
