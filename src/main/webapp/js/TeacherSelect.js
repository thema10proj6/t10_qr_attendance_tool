//
function CourseSelector() {
    var teacher = document.getElementById("teacher_selector").value;
    var coursefield = document.getElementById("course_selector");
    coursefield.innerHTML = "";

    $.get("course", {teacher: teacher}, function(data){
        var courses = data.split(",");
        for (i = 0; i < courses.length; i++) {
            coursefield.innerHTML = coursefield.innerHTML + "<option value=" + courses[i]+ ">" + courses[i] + "</option>";
        }
    })
}

function DaySelector() {
    var course = document.getElementById("course_selector").value;
    var datefield = document.getElementById("day_selector");
    datefield.innerHTML = "";

    $.get("day", {course: course}, function(data){
        var dates = data.split(",");
        for(i = 0; i < dates.length; i++) {
            var date = dates[i].split(".");
            datefield.innerHTML = datefield.innerHTML + "<option value=" + date[1] + ">" + date[0] + "</option>"




        }
    })

}