/**
 * Generates QR code using values provided
 */
$("document").ready(
    function QrGenerator() {
        // Values from input fields
        var teacher = document.getElementById("teacher").value;
        var course = document.getElementById("course").value;
        var unix = document.getElementById("time").value;
        var maxcount = document.getElementById("maxcount").value;
        var hashString = document.getElementById("hash").value;
        var websiteURL = document.getElementById("webURL");

        // website link created by using the variables
        var QRlink =
            websiteURL.value + "/attendance?&teachercode=" + teacher + "&coursecode=" + course +
            "&unixdate1=" + unix + "&hashstring=" + hashString;

        // QR code generated from the URL
        var qrcode = new QRCode("qrcode", { width:600, height:600});

        qrcode.makeCode(QRlink);

        // Every QR code instance get sent to a servlet to be added to the database
        $.get("qrinstance",{QRLink: QRlink, unix: unix, teacher: teacher, course: course, maxcount: maxcount, hashstring: hashString},
            function(data){})
    }
);

