/**
 * Creates a unix timestamp when called
 */
function unix() {
    var timevar = document.getElementById("hidden_unixdate");
    timevar.value = Math.round((new Date()).getTime() / 1000);
}