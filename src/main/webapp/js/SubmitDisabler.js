/**
 * Disables the submit button once the maximum amount of students have been reached
 */
$(document).ready(function() {
    var hashstring = document.getElementById("hidden_hashstring").value;
    // max student amount
    $.get("maxcount",{hash: hashstring},function(data) {
        maxcount = data;
    });

    // compares the student count with the maximum amount
    var compare_count = function(data) {
        if (data >= parseInt(maxcount, 10)) {
            document.getElementById("student_submit").disabled = true;
            document.getElementById("full_attendance").innerHTML = "Maximum amount of attendants reached.";
        }
    };

    //gets student count
    var get_count = function () {
        $.when(
            $.get("counter",{hash: hashstring}, function (data) {
                count = data;
            })
        ).then(function () {
            compare_count(count);
        })


    };
    //refreshes the get_count function every time interval
    window.setInterval(function () {
        get_count();
    }, 500); //in milliseconds
});