<%--
  Created by IntelliJ IDEA.
  User: kcduong
  Date: 14-1-19
  Time: 13:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-left" href="index.jsp"><img src="includes/QRnav.png"></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#my-navbar" aria-expanded="false">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="my-navbar">
                <ul class="nav navbar-nav">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="download">Download</a> </li>
                </ul>
                <ul class="nav navbar-nav navbar-right" action="logout.do">
                    <li><a href="logout.do"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>

                </ul>
            </div>
        </div>
    </nav>
</div>
