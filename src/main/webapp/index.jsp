<%--
  Created by IntelliJ IDEA.
  User: kcduong
  Date: 4-12-18
  Time: 13:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Attendence Tool</title>
    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css"/>

    <script type="text/javascript" src="js/lib/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <script type="text/javascript" src="js/lib/qrcode.min.js"></script>
    <script src="js/UnixGenerator.js" type="application/javascript"></script>
    <script src="js/QrGenerator.js" type="application/javascript"></script>
    <script src="js/ScrollFieldDisabler.js" type="application/javascript"></script>

</head>
<body>
<c:choose>
    <c:when test="${sessionScope.user != null}">
        <jsp:include page="includes/Header.jsp"></jsp:include>
        <div class="centered-fields">
            <div class="container" align="center">
                <h2>Welcome to the Attendance taking tool <br />
                    Fill in the form below to create a QR code <br /> </h2>
                <h3>Head over to the Download page if you wish to download attendance data</h3> <br />
                <h3>Create QR code</h3>

                <form action="create.do" method="post" onsubmit="unix()">
                    <div class="form-group">
                        <label for="Teacher"> Teacher code:</label>
                        <input class="form-control resize-field" id="Teacher" type="text" name="Teacher"
                               style="text-transform:uppercase" required title="Your 4-letter teacher code, e.g. ABCD"
                               placeholder="Teacher code" />
                    </div>
                    <div class="form-group">
                        <label for="subject"> Course code:</label>
                        <input class="form-control resize-field" id="subject" type="text" name="subject"
                               style="text-transform:uppercase" required title="The course code" placeholder="Course code"/>
                    </div>
                    <div class="form-group">
                        <label for="maxcount"> Amount of students present:</label>
                        <input class="form-control resize-field" id="maxcount" type="number" pattern="[0-9]*" min="0"
                               onkeydown="return event.keyCode !== 69" name="maxcount" required
                               title="Total amount of students currently present" placeholder="Amount of students"/>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="unixdate" id="hidden_unixdate">
                        <input class="form-control resize-field" type="submit" value="Generate QR code"/>
                    </div>
                </form>
            </div>
        </div>

    </c:when>
    <c:otherwise>
        <jsp:include page="includes/EmptyHeader.jsp"></jsp:include>

        <div class="centered-fields">
            <div class="container" align="center">
                <h2>Please log in first in order to use the Attendance Taking tool</h2>

                <form action="login.do" method="post">
                    <div class="row">
                        <div class="form-group">
                            <label for="password_field">Password:</label><br/>
                            <input class="form-control resize-field" id="password_field" type="password"
                                   name="password" required title="Your password" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="login_field"></label>
                            <input class="form-control resize-field" type="submit" value="Log in">
                        </div>
                    </div>
                </form>

                <p id="error">${requestScope.error}</p>
            </div>
        </div>
    </c:otherwise>
</c:choose>
</body>
</html>
