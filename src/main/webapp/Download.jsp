<%--
  Created by IntelliJ IDEA.
  User: rjonker
  Date: 14-1-19
  Time: 13:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Download Attendance Data</title>
    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css"/>

    <script type="text/javascript" src="js/lib/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="js/TeacherSelect.js" type="application/javascript"></script>
</head>
<body>

<jsp:include page="includes/Header.jsp"></jsp:include>

<div class="container" align="center">
    <legend>In order to download attendance data, please select the teacher, course, and date<br />
    You can download the entire attendance taking data with &#34;Download All&#34;</legend>
    <table>
        <tr>
            <td>Select teacher:</td>
            <td>
                <select class="form-control resize-field" name="teacher" onclick="CourseSelector()" id="teacher_selector" required>
                    <option disabled selected value> -- select a teacher first -- </option>
                    <c:forEach var="teacher" items="${requestScope.teachers}">
                        <option value="${teacher}">${teacher}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td>Select course: </td>
            <td>
                <select class="form-control resize-field" name="course" onclick="DaySelector()" id="course_selector" required></select>
            </td>
        </tr>
        <tr>
            <td>Select date:</td>
            <td>
                <select class="form-control resize-field" name="hashString" id="day_selector" form="download" required></select>
            </td>
        </tr>
    </table>
    <br>
    <table>
        <tr>
            <td>
                <form id="download" action="download.do" method="get">
                    <input class="form-control resize-field" type="submit" value="Download"/>
                </form>
            </td>
            <td>
                <form id="download_all" action="download.do" method="get">
                    <input type="hidden" name="amount" value="1">
                    <input class="form-control resize-field" type="submit" value="Download all"/>
                </form>
            </td>
        </tr>
    </table>
</div>

</body>
</html>
