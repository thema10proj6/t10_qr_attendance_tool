<%--
  Created by IntelliJ IDEA.
  User: rinze
  Date: 12/12/2018
  Time: 09:11
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>QR code page</title>
    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css"/>

    <script type="text/javascript" src="js/lib/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="js/lib/qrcode.min.js"></script>
    <script type="text/javascript" src="js/QrGenerator.js"></script>
    <script src="js/Counter.js" type="application/javascript"></script>
</head>
<body>
    <jsp:include page="includes/Header.jsp"></jsp:include>

    <div class="container-student">
        <div class="container" align="center">
            <p style="font-size: 30px;">Please scan this code</p>
            <div id="qrcode"></div>
            <br/>
                <table align="center">
                    <tr>
                        <td><div id="studentcounter">0</div></td>
                        <td>&nbsp;/ ${requestScope.maxcount} submissions</td>
                    </tr>
                </table>
            <div>
                <form action="resultpage.do" method="get">
                    <input type="hidden" name="te" id="teacher" value="${requestScope.teachercode}">
                    <input type="hidden" name="co" id="course" value="${requestScope.coursecode}">
                    <input type="hidden" name="ti" id="time" value="${requestScope.unixdate}">
                    <input type="hidden" name="mc" id="maxcount" value="${requestScope.maxcount}">
                    <input type="hidden" name="hs" id="hash" value="${requestScope.hashString}">
                    <input type="hidden" name="URL" id="webURL" value="${requestScope.websiteURL}">
                    <input class="form-control resize-field" type="submit" value="Statistics page" formtarget="_blank">
                </form>
            </div>
        </div>
    </div>
</body>
</html>