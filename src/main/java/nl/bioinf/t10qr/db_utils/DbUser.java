/*
 * Copyright (c) 2018 Kim Chau Duong
 * All rights reserved
 */
package nl.bioinf.t10qr.db_utils;

/**
 * This class creates a database user instance in order to connect with the MySQL database.
 *
 * @author Kim Chau Duong
 * @version 1.0
 */
public class DbUser {
    public String host;
    public String databaseName;
    public String userName;
    public String databasePassword;

    public DbUser(String host, String database, String userName, String databasePassword) {
        this.host = host;
        this.databaseName = database;
        this.userName = userName;
        this.databasePassword = databasePassword;
    }

    public String getHost() {
        return host;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getUserName() {
        return userName;
    }

    public String getDatabasePassword() {
        return databasePassword;
    }
}
