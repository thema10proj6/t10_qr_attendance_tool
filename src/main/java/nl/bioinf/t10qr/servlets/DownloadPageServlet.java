package nl.bioinf.t10qr.servlets;

import nl.bioinf.t10qr.dao.DatabaseException;
import nl.bioinf.t10qr.dao.MyAppDao;
import nl.bioinf.t10qr.dao.MyAppDaoMySQL;
import nl.bioinf.t10qr.db_utils.DbUser;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Servlet that serves as a link to the download page of the webapp
 * all the information of this servlet will be sent to Download.jsp
 *
 * Copyright (c) 2018 Rinze-Pieter Jonker
 * All rights reserved
 */
@WebServlet(name = "DownloadPageServlet", urlPatterns = "/download")
public class DownloadPageServlet extends HttpServlet {
    private MyAppDao dao;

    @Override
    public void init() throws ServletException {
        super.init();
        dao = new MyAppDaoMySQL();
        try {
            DbUser user = new DbUser(getServletContext().getInitParameter("sqlHost"),
                    getServletContext().getInitParameter("sqlDatabase"),
                    getServletContext().getInitParameter("sqlUser"),
                    getServletContext().getInitParameter("sqlPassword"));
            dao.connect(user);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void destroy() {
        super.destroy();
        try {
            dao.disconnect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleRequest(request, response);
    }

    /**
     * Handles the response and request of both the get and the post
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ArrayList<String> teachers = JobRunner();
        request.setAttribute("teachers", teachers);

        RequestDispatcher view = request.getRequestDispatcher("Download.jsp");
        view.forward(request, response);
    }

    /**
     * runs the mysql function and deals with the Exception
     * @return Arraylist with teacher names (String)
     */
    private ArrayList<String> JobRunner() {

        ArrayList<String> result = new ArrayList<String>();

        try {
            result = dao.getAllTeachers();

        } catch (DatabaseException ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
