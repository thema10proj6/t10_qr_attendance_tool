package nl.bioinf.t10qr.servlets;

import nl.bioinf.t10qr.dao.DatabaseException;
import nl.bioinf.t10qr.dao.MyAppDao;
import nl.bioinf.t10qr.dao.MyAppDaoMySQL;
import nl.bioinf.t10qr.db_utils.DbUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet that sends all the QR code instance information into the database
 *
 * Copyright (c) 2018 Kim Chau Duong
 * All rights reserved
 */
@WebServlet(name = "QRInstanceServlet", urlPatterns = "/qrinstance")
public class QRInstanceServlet extends HttpServlet {
    private MyAppDao dao;

    @Override
    public void init() throws ServletException {
        super.init();
        dao = new MyAppDaoMySQL();
        try {
            DbUser user = new DbUser(getServletContext().getInitParameter("sqlHost"),
                    getServletContext().getInitParameter("sqlDatabase"),
                    getServletContext().getInitParameter("sqlUser"),
                    getServletContext().getInitParameter("sqlPassword"));
            dao.connect(user);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void destroy() {
        super.destroy();
        try {
            dao.disconnect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String qrURL = request.getParameter("QRLink");
        String unixOne = request.getParameter("unix");
        String teacher = request.getParameter("teacher");
        String course = request.getParameter("course");
        String maxCount = request.getParameter("maxcount");
        String hashString = request.getParameter("hashstring");

        try {
            dao.insertQRInstance(qrURL, unixOne, teacher, course, maxCount, hashString);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }
}
