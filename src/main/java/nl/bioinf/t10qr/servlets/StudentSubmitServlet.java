package nl.bioinf.t10qr.servlets;

import nl.bioinf.t10qr.dao.DatabaseException;
import nl.bioinf.t10qr.dao.MyAppDao;
import nl.bioinf.t10qr.dao.MyAppDaoMySQL;
import nl.bioinf.t10qr.db_utils.DbUser;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet that handles the student number submission.
 * Checks if student has already recorded attendance, if the maximum amount of students have been reached yet
 * and then adds this instance into the database.
 *
 * Copyright (c) 2018 Kim Chau Duong
 * All rights reserved
 */
@WebServlet(name = "StudentSubmitServlet", urlPatterns = "/attendance_taken")
public class StudentSubmitServlet extends HttpServlet {
    private MyAppDao dao;

    @Override
    public void init() throws ServletException {
        super.init();
        dao = new MyAppDaoMySQL();
        try {
            DbUser user = new DbUser(getServletContext().getInitParameter("sqlHost"),
                    getServletContext().getInitParameter("sqlDatabase"),
                    getServletContext().getInitParameter("sqlUser"),
                    getServletContext().getInitParameter("sqlPassword"));
            dao.connect(user);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void destroy() {
        super.destroy();
        try {
            dao.disconnect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleRequest(request,response);

    }
    private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String unix1 = request.getParameter("unixdate1");
        String unix2 = request.getParameter("unixdate2");
        String course = request.getParameter("coursecode");
        String teacher = request.getParameter("teachercode");
        String student = request.getParameter("studentno");
        String hashString = request.getParameter("hashstring");

        RequestDispatcher view = null;

        try{
            int currentCount = dao.countCheckIns(hashString);
            int maxCount = dao.getMaxCount(hashString);
            boolean existingStudent = dao.checkCheckIns(student, hashString);
            if (existingStudent){
                final String errorMessage = "Already recorded attendance" ;
                request.setAttribute("error", errorMessage);
                view = request.getRequestDispatcher("StudentForm.jsp");

            } else if (currentCount >= maxCount) {
                final String errorMessage = "Maximum amount of attendants reached.";
                request.setAttribute("full", errorMessage);
                view = request.getRequestDispatcher("StudentForm.jsp");
            }
            else {
                try {
                    dao.insertCheckIn(unix1, unix2, course, teacher, student, hashString);
                } catch (DatabaseException e) {
                    e.printStackTrace();
                }

                view = request.getRequestDispatcher("Enrolled.jsp");
            }
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

        view.forward(request, response);
    }
}
