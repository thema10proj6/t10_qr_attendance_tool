package nl.bioinf.t10qr.servlets;

import javax.servlet.RequestDispatcher;
import java.io.IOException;

/**
 * Servlet that creates a page that the QR code can redirect to.
 * All the info from this page will be forwarded to "/StudentForm.jsp"
 *
 * Copyright (c) 2018 Kim Chau Duong
 * All rights reserved
 */
@javax.servlet.annotation.WebServlet(name = "AttendanceServlet", urlPatterns = "/attendance")
public class AttendanceServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("StudentForm.jsp");
        view.forward(request, response);
    }
}
