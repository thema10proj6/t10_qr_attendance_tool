package nl.bioinf.t10qr.servlets;

import nl.bioinf.t10qr.User.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Servlet that handles the login of the teacher
 * this servlet will send all the information to index.jsp
 *
 * Copyright (c) 2018 Rinze-Pieter Jonker & Kim Chau Duong
 * All rights reserved
 */
@WebServlet(name = "LoginServlet", urlPatterns = "/login.do")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        // Creates Cookie so the user won't get logged out immediately
        if (request.getParameter("JSESSIONID") != null) {
            Cookie userCookie = new Cookie("JSESSIONID", request.getParameter("JSESSIONID"));
            response.addCookie(userCookie);
        } else {
            String sessionId = session.getId();
            Cookie userCookie = new Cookie("JSESSIONID", sessionId);
            response.addCookie(userCookie);
        }

        RequestDispatcher view;
        User user = (User) session.getAttribute("user");
        if (user != null) {
            view = request.getRequestDispatcher("index.jsp");
        } else {
            String password = request.getParameter("password");

            final User authenticatedUser = authenticate(password);
            if (authenticatedUser != null){
                session.setAttribute("user", authenticatedUser);
                view = request.getRequestDispatcher("index.jsp");
            } else {
                final String errorMessage = "Wrong password or username" ;
                request.setAttribute("error", errorMessage);
                view = request.getRequestDispatcher("index.jsp");
            }
        }
        view.forward(request, response);

    }

    private User authenticate(String password) {
        // password configurable in web.xml
        String configuredPassword = getServletContext().getInitParameter("configured_password");
        if (password.equals(configuredPassword)) {
            return new User(password);
        } else {
            return null;
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
