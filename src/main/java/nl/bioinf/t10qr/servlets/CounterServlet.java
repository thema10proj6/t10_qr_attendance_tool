package nl.bioinf.t10qr.servlets;

import nl.bioinf.t10qr.dao.DatabaseException;
import nl.bioinf.t10qr.dao.MyAppDao;
import nl.bioinf.t10qr.dao.MyAppDaoMySQL;
import nl.bioinf.t10qr.db_utils.DbUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet that checks in the database how many students have scanned the QR code and submitted their student numbers.
 *
 * Copyright (c) 2018 Kim Chau Duong
 * All rights reserved
 */
@WebServlet(name = "CounterServlet", urlPatterns = "/counter")
public class CounterServlet extends HttpServlet {
    private MyAppDao dao;

    @Override
    public void init() throws ServletException {
        super.init();
        dao = new MyAppDaoMySQL();
        try {
            DbUser user = new DbUser(getServletContext().getInitParameter("sqlHost"),
                    getServletContext().getInitParameter("sqlDatabase"),
                    getServletContext().getInitParameter("sqlUser"),
                    getServletContext().getInitParameter("sqlPassword"));
            dao.connect(user);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void destroy() {
        super.destroy();
        try {
            dao.disconnect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String hash = request.getParameter("hash");

        try {
            int studentCount = dao.countCheckIns(hash);

            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().print(studentCount);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }


    }
}
