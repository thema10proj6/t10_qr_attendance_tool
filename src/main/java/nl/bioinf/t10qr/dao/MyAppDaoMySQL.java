package nl.bioinf.t10qr.dao;

import nl.bioinf.t10qr.Functions.UnixToTime;
import nl.bioinf.t10qr.db_utils.DbUser;

import java.sql.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.ArrayList;


public class MyAppDaoMySQL implements MyAppDao{
    Connection connection;

    @Override
    public void connect(DbUser user) throws DatabaseException {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            String dbUrl = "jdbc:mysql://" + user.getHost() + "/" + user.getDatabaseName();
            String dbUser = user.getUserName();
            String dbPassword = user.getDatabasePassword();
            connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
        } catch (Exception e){
            throw new DatabaseException("There's something wrong with the database, see Exception cause",
                    e.getCause());
        }
    }

    @Override
    public void insertCheckIn(String unixOne, String unixTwo, String course, String teacher, String student, String hashString) throws DatabaseException  {
        try{
            String insertQuery =
                    "INSERT INTO CheckIns (unix_one, unix_two, course_code, teacher, student, hash_string) "
                            + " VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement ps = connection.prepareStatement(insertQuery);
            ps.setInt(1, Integer.parseInt(unixOne));
            ps.setInt(2, Integer.parseInt(unixTwo));
            ps.setString(3, course.toUpperCase());
            ps.setString(4, teacher.toUpperCase());
            ps.setInt(5, Integer.parseInt(student));
            ps.setString(6, hashString);
            ps.executeUpdate();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }
    }


    @Override
    public void insertQRInstance(String qrURL, String unixOne, String teacher, String course, String maxCount, String hashString) throws DatabaseException {
        try{
            String insertQuery =
                    "INSERT INTO QRInstances (qr_url, unix_one, teacher, course_code, max_count, hash_string)"
                        + "VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement ps = connection.prepareStatement(insertQuery);
            ps.setString(1,qrURL);
            ps.setInt(2, Integer.parseInt(unixOne));
            ps.setString(3, teacher.toUpperCase());
            ps.setString(4, course.toUpperCase());
            ps.setInt(5, Integer.parseInt(maxCount));
            ps.setString(6,hashString);
            ps.executeUpdate();
            ps.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    ex.getCause());
        }
    }

    @Override
    public int countCheckIns(String hashString) throws DatabaseException {
        try{
            String fetchQuery =
                    "SELECT COUNT(checkin_id) FROM CheckIns WHERE hash_string = ?";
            PreparedStatement ps = connection.prepareStatement(fetchQuery);
            ps.setString(1, hashString);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                return rs.getInt(1);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DatabaseException("Something is wrong with the database, see cause Exception",
                    e.getCause());
        }

        return 0;
    }

    @Override
    public boolean checkCheckIns(String student, String hashString) throws DatabaseException {
        try {
            String fetchQuery =
                    "SELECT EXISTS (SELECT * FROM CheckIns WHERE student = ? and hash_string = ?);";
            PreparedStatement ps = connection.prepareStatement(fetchQuery);
            ps.setString(1, student);
            ps.setString(2, hashString);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                return rs.getBoolean(1);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public int getMaxCount(String hashString) throws DatabaseException {
        try {
            String fetchQuery =
                    "SELECT max_count FROM QRInstances WHERE hash_string = ?";
            PreparedStatement ps = connection.prepareStatement(fetchQuery);
            ps.setString(1, hashString);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                return rs.getInt(1);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }


    @Override
    public ArrayList<String> getAllTeachers() throws DatabaseException {
        ArrayList<String> result = new ArrayList<String>();
        try {
            String query = "SELECT teacher FROM QRInstances";
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String temp = rs.getString(1);
                if (!result.contains(temp)) {
                    result.add(temp);
                }
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result;
    }

    @Override
    public String getAllCourses(String teacher) throws DatabaseException{
        String result = "";
        try {
            String query = "SELECT course_code FROM QRInstances WHERE teacher = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, teacher);
            ResultSet rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                String temp = rs.getString(1);
                if (!result.contains(temp)) {
                    if (i == 0) {
                        result = temp;
                    } else {
                        result = String.join(",", result, temp);
                    }
                }
                i++;
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result;
    }

    @Override
    public String getAllDates(String course) throws DatabaseException{
        String result = "";
        try {
            String query = "SELECT unix_one, hash_string FROM QRInstances WHERE course_code = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, course);
            ResultSet rs = ps.executeQuery();

            int i = 0 ;
            while (rs.next()) {
                String date = UnixToTime.GetDateFromUnix(rs.getInt(1));
                String hash = rs.getString(2);
                String combine = String.join(".", date, hash);
                if (i == 0) {
                    result = combine;
                } else {
                    if (!result.contains(combine)){
                        result = String.join(",", result, combine);
                    }
                }
                i++;
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result;
    }

    @Override
    public ArrayList<String> DatabaseDump(String hashString) throws DatabaseException{
        ArrayList<String> result = new ArrayList<>();
        result.add("checkin_id, unix_one, unix_two, course_code, teacher, student, hash_string");
        try {
            String query;
            PreparedStatement ps;
            if (hashString.equals("NVT")){
                query = "SELECT * FROM CheckIns";
                ps = connection.prepareStatement(query);
            } else {
                query = "SELECT * FROM CheckIns WHERE hash_string = ?";
                ps = connection.prepareStatement(query);
                ps.setString(1, hashString);
            }
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                String temp = String.join(", ",
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7));
                result.add(temp);
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result;

    }

    @Override
    public void disconnect() throws DatabaseException {
        try {
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
