package nl.bioinf.t10qr.dao;

import nl.bioinf.t10qr.db_utils.DbUser;

import java.util.ArrayList;
/**
 * This is the interface for the MyAppDaoMySQL file.
 *
 * @author Kim Chau Duong
 * @version 1.0
 */
public interface MyAppDao {
    /**
     * connects to the data layer.
     * @throws DatabaseException
     */
    void connect(DbUser user) throws DatabaseException;


    /**
     * inserts a new check in.
     * @param unixOne
     * @param unixTwo
     * @param course
     * @param teacher
     * @param student
     * @param hashString
     * @throws DatabaseException
     */
    void insertCheckIn(String unixOne, String unixTwo, String course, String teacher, String student, String hashString) throws DatabaseException;

    /**
     * inserts every QR code instance that gets made.
     * @param qrURL
     * @param unixOne
     * @param teacher
     * @param course
     * @param maxCount
     * @throws DatabaseException
     */
    void insertQRInstance(String qrURL, String unixOne, String teacher, String course, String maxCount, String hashString) throws DatabaseException;


    /**
     * counts student count depending on the hash string.
     * @param hashString
     * @throws DatabaseException
     * @return Integer
     */
    int countCheckIns(String hashString) throws DatabaseException;

    /**
     * Checks if student has already submitted their studentnumber for this QR code instance.
     * @param student
     * @param hashString
     * @throws DatabaseException
     * @retun boolean
     */
    boolean checkCheckIns(String student, String hashString) throws DatabaseException;

    /**
     * Gets max student count from database when given a hash string.
     * @param hashString
     * @throws DatabaseException
     * @return Integer max student amount
     */
    int getMaxCount(String hashString) throws DatabaseException;

    /**
     * Gets all the teachers from the database
     * @return
     * @throws DatabaseException
     * @return Arraylist with all the teachers
     */
    ArrayList<String> getAllTeachers() throws DatabaseException;

    /**
     * returns all the courses with a specific teacher form the database
     * @param teacher name of the teacher
     * @return , separated string with all the courses
     * @throws DatabaseException
     */
    String getAllCourses(String teacher) throws DatabaseException;

    /**
     * returns all the dates and hashstrings with a specific course code from the database
     * @param Course the course code
     * @return a string with all the dates and hashstrings
     *          example (date1.hashstring1,date2.hashstring2)
     * @throws DatabaseException
     */
    String getAllDates(String Course) throws DatabaseException;

    /**
     * dumps all the lines for a given hashstring into a arraylist
     * @param hashString hashstring for a specific college
     * @return arraylist with all the data
     * @throws DatabaseException
     */
    ArrayList<String> DatabaseDump(String hashString) throws DatabaseException;

    /**
     * closes the connection to the data layer and frees resources.
     * @throws DatabaseException
     */
    void disconnect() throws DatabaseException;




}