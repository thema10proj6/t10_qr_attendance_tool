package nl.bioinf.t10qr.User;

import java.util.Objects;

/**
 * Creates User instance for the logging in process
 */
public class User {
//    private String teacherCode;
    private String password;

    public User() {}

    public User(String password) {
        this.password = password;
    }


    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "User{" +
                "password='" + password + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getPassword(), user.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPassword());
    }
}
