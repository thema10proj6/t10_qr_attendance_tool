package nl.bioinf.t10qr.DatabaseInstance;

import java.util.Objects;

public class DatabaseInstance {
    public String hashString;
    public String unixTime;
    public String teacher;


    public DatabaseInstance(String hashString, String unixTime, String teacher) {
        this.hashString = hashString;
        this.unixTime = unixTime;
        this.teacher = teacher;
    }

    public String getHashString() {
        return hashString;
    }

    public String getUnixTime() {
        return unixTime;
    }

    public String getTeacher() {
        return teacher;
    }

    @Override
    public String toString() {
        return "DatabaseInstance{" +
                "hashString='" + hashString + '\'' +
                ", unixTime=" + unixTime +
                ", teacher='" + teacher + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DatabaseInstance that = (DatabaseInstance) o;
        return Objects.equals(unixTime, that.unixTime) &&
                Objects.equals(hashString, that.hashString) &&
                Objects.equals(teacher, that.teacher);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hashString, unixTime, teacher);
    }
}
